package com.trainee.musicplayerapp

import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.widget.Button
import java.io.IOException
import java.lang.NullPointerException

class MediaPlayerService : Service(),
    MediaPlayer.OnPreparedListener,
    MediaPlayer.OnCompletionListener,
    MediaPlayer.OnErrorListener {
    lateinit var pause: Button
    lateinit var play: Button
    private val iBinder: IBinder = LocalBinder()
    private lateinit var mediaPlayer: MediaPlayer
    private var mediaFile = ""
    private var resumePosition = 0

    private fun initMediaPlayer() {
        mediaPlayer = MediaPlayer()
        mediaPlayer.setOnPreparedListener(this)
        mediaPlayer.setOnCompletionListener(this)

        mediaPlayer.reset()

        mediaPlayer.setAudioAttributes(
            AudioAttributes.Builder()
                .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                .build()
        )
        try {
            mediaPlayer.setDataSource(mediaFile)
        } catch (ei: IOException) {
            stopSelf()
        }
        mediaPlayer.prepareAsync()
    }

    override fun onBind(intent: Intent?) = iBinder

    override fun onPrepared(mp: MediaPlayer?) {
        playMedia()
    }

    override fun onCompletion(mp: MediaPlayer?) {
        stopMedia()
        stopSelf()
        pause.isEnabled = false
        play.isEnabled = true
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int) = false

    inner class LocalBinder : Binder() {
        fun getService() = this@MediaPlayerService
    }

    private fun playMedia() {
        if (!mediaPlayer.isPlaying) {
            mediaPlayer.start()
        }
    }

    private fun stopMedia() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.stop()
        }
    }

    fun resumeMedia() {
        if (!mediaPlayer.isPlaying) {
            mediaPlayer.seekTo(resumePosition)
            mediaPlayer.start()
        }
    }

    fun pauseMedia() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            resumePosition = mediaPlayer.currentPosition
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            mediaFile = intent?.getStringExtra("media").toString()
        } catch (e: NullPointerException) {
            stopSelf()
        }
        if (mediaFile != "") {
            initMediaPlayer()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopMedia()
        mediaPlayer.release()
    }
}