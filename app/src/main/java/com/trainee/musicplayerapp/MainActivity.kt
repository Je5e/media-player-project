package com.trainee.musicplayerapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.trainee.musicplayerapp.MediaPlayerService.LocalBinder

class MainActivity : AppCompatActivity() {
    lateinit var pauseButton: Button
    lateinit var playButton: Button
    private var isPause = false
    lateinit var player: MediaPlayerService
    var serviceBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_screen)

        // Find controls
        playButton = findViewById(R.id.play_button)
        pauseButton = findViewById(R.id.pause_button)
        val detailButton = findViewById<Button>(R.id.detail_button)
        val editText = findViewById<EditText>(R.id.url_song_editText)

        playButton.setOnClickListener()
        {

            if (isPause) {
                player.resumeMedia()
                isPause = false
            } else {
                // mediaPlayer = MediaPlayer.create(this, R.raw.happy)
                playAudio(editText.text.toString())
            }
            pauseButton.isEnabled = true
            playButton.isEnabled = false
        }

        pauseButton.setOnClickListener {
            player.pauseMedia()
            pauseButton.isEnabled = false
            playButton.isEnabled = true
            isPause = true
        }

        detailButton.setOnClickListener {
            val intent = Intent(
                this,
                SongDetailSong::class.java,
            )
            startActivity(intent)
        }
    }

    private fun playAudio(media: String) {
        //Check is service is active
        if (!serviceBound) {
            val playerIntent = Intent(this@MainActivity, MediaPlayerService::class.java)
            playerIntent.putExtra("media", media)
            startService(playerIntent)

            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE)

        }
    }

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as LocalBinder
            player = binder.getService()
            serviceBound = true
            player.play = playButton
            player.pause = pauseButton
        }

        override fun onServiceDisconnected(name: ComponentName) {
            serviceBound = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (serviceBound) {
            unbindService(serviceConnection)
            //service is active
            player.stopSelf()
        }

    }
}